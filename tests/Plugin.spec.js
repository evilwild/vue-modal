const Vue = require('vue');
const Plugin = require('../dist/index.js').default;

describe('Plugin core', () => {
  Vue.use(Plugin);
  const vm = new Vue({
    render: (h) => h('div'),
  }).$mount();
  describe('$modal', () => {
    const $modal = vm.$modal;
    it('should be defined', () => {
      expect($modal).toBeDefined();
    });
    it('should have spawn()', () => {
      expect($modal.spawn).toBeDefined();
      expect($modal.spawn).toBeInstanceOf(Function);
    });
    it('should have kill()', () => {
      expect($modal.kill).toBeDefined();
      expect($modal.kill).toBeInstanceOf(Function);
    });
    it('should have killAll()', () => {
      expect($modal.killAll).toBeDefined();
      expect($modal.killAll).toBeInstanceOf(Function);
    });
    it('should have show()', () => {
      expect($modal.show).toBeDefined();
      expect($modal.show).toBeInstanceOf(Function);
    });
    it('should have hide()', () => {
      expect($modal.hide).toBeDefined();
      expect($modal.hide).toBeInstanceOf(Function);
    });
    it('should emit v-modal::spawn', () => {
      const someModal = Vue.extend({
        name: 'SomeModal',
        render: (h) => ('div', ['it is modal']),
      });
      let emitted = false;
      vm.$modal.ctx.modalContainer.$once('v-modal::spawn', (payload) => {
        emitted = true;
      });
      $modal.spawn(someModal, { test: 'foo' });
      expect(emitted).toBeTruthy();
    });
    it('should emit v-modal::kill', () => {
      let emitted = false;
      vm.$modal.ctx.modalContainer.$once('v-modal::kill', () => {
        emitted = true;
      });
      $modal.kill();
      expect(emitted).toBeTruthy();
    });
    it('should emit v-modal::kill-all', () => {
      let emitted = false;
      vm.$modal.ctx.modalContainer.$once('v-modal::kill-all', () => {
        emitted = true;
      });
      $modal.killAll();
      expect(emitted).toBeTruthy();
    });
  });
  describe('ModalsContainer instance', () => {
    const modalContainer = vm.$modal.ctx.modalContainer;
    it('should be defined', () => {
      expect(modalContainer).toBeDefined();
    });
    it('should have add()', () => {
      expect(modalContainer.add).toBeDefined();
      expect(modalContainer.add).toBeInstanceOf(Function);
    });
    it('should have remove()', () => {
      expect(modalContainer.remove).toBeDefined();
      expect(modalContainer.remove).toBeInstanceOf(Function);
    });
    it('should have count property', () => {
      expect(modalContainer.count).toBeDefined();
    });
  });
});
