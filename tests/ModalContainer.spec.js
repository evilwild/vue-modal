const Vue = require('vue');
const Plugin = require('../dist/index.js').default;

describe('ModalContainer component', () => {
  Vue.use(Plugin);
  const vm = new Vue({
    render: (h) => h('div'),
  }).$mount();

  const modalContainer = vm.$modal.ctx.modalContainer;
  const SomeModal = Vue.extend({
    name: 'SomeModal',
    render: (h) => h('div', ['it is modal']),
  });
  it('should add modal', () => {
    modalContainer.add(SomeModal);
    expect(modalContainer.count).toBe(1);
  });
  it('should remove modal', () => {
    modalContainer.add(SomeModal);
    const { id } = modalContainer.modals[1];
    modalContainer.remove(id);
    expect(modalContainer.count).toBe(1);
  });
  it('should emit v-modal::add', () => {
    let emitted = false;
    modalContainer.$once('v-modal::add', () => {
      emitted = true;
    });
    modalContainer.add(SomeModal);
    vm.$nextTick(() => {
      expect(emitted).toBeTruthy();
    });
  });
  it('should emit v-modal::remove', () => {
    let emitted = false;
    modalContainer.$once('v-modal::remove', () => {
      emitted = true;
    });
    const { id } = modalContainer.modals[0];
    modalContainer.remove(id);
    expect(emitted).toBeTruthy();
  });
  it('should pass some data to modal', () => {
    modalContainer.modals = [];
    modalContainer.add(SomeModal, {
      test: 'true',
    });
    expect(modalContainer.modals[0].props.test).toBe('true');
  });
});
