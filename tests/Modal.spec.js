const Vue = require('vue');
const Plugin = require('../dist/index').default;

describe('Modal component', () => {
  Vue.use(Plugin);
  const vm = new Vue({
    name: 'Root',
    render: (h) => h('div'),
  });
  const someModal = Vue.extend({
    name: 'SomeModal',
    render(h) {
      const testData = this.$attrs.foo;
      const textContent = `some modal ${testData}`;
      return h('div', [textContent]);
    },
  });

  vm.$modal.spawn(someModal, {
    test: 'true',
    foo: 'bar',
  });

  function getModal() {
    return vm.$modal.ctx.modalContainer.$children[0];
  }

  it('should exist on ModalContainer.vue', () => {
    const vmSomeModal = getModal();
    expect(vmSomeModal).toBeDefined();
  });
  it('should has visible prop', () => {
    const vmSomeModal = getModal();
    expect(vmSomeModal.visible).toBeDefined();
  });
  it('should init with visible = true', () => {
    const vmSomeModal = getModal();
    expect(vmSomeModal.visible).toBeTruthy();
  });
  it('should has id prop', () => {
    const vmSomeModal = getModal();
    expect(vmSomeModal.$props.id).toBeDefined();
  });
  it('should has show()', () => {
    const vmSomeModal = getModal();
    expect(vmSomeModal.show).toBeDefined();
  });
  it('should has hide()', () => {
    const vmSomeModal = getModal();
    expect(vmSomeModal.hide).toBeDefined();
  });
  it('should be visible', () => {
    const vmSomeModal = getModal();
    vmSomeModal.show();
    expect(vmSomeModal.visible).toBeTruthy();
  });
  it('should be not visible', () => {
    const vmSomeModal = getModal();
    vmSomeModal.hide();
    expect(vmSomeModal.visible).toBeFalsy();
  });
  it('should emit v-modal::before-show', () => {
    let emitted = false;
    const vmSomeModal = getModal();
    vmSomeModal.$root.$once('v-modal::before-show', () => {
      emitted = true;
    });
    vmSomeModal.show();
    expect(emitted).toBeTruthy();
  });
  it('should emit v-modal::show', () => {
    const vmSomeModal = getModal();
    let respond;
    let modalId = vmSomeModal.$props.id;
    vmSomeModal.$root.$once('v-modal::show', (payload) => {
      respond = payload.id;
    });
    vmSomeModal.show();
    expect(respond).toBe(modalId);
  });
  it('should emit v-modal::before-hide', () => {
    let emitted = false;
    const vmSomeModal = getModal();
    vmSomeModal.$root.$once('v-modal::before-hide', () => {
      emitted = true;
    });
    vmSomeModal.hide();
    expect(emitted).toBeTruthy();
  });
  it('should emit v-modal::hide', () => {
    const vmSomeModal = getModal();
    let respond;
    let modalId = vmSomeModal.$props.id;
    vmSomeModal.$root.$once('v-modal::hide', (payload) => {
      respond = payload.id;
    });
    vmSomeModal.hide();
    expect(respond).toBe(modalId);
  });
  it('should receive v-modal::init-show', () => {
    let emitted = false;
    const vmSomeModal = getModal();
    vmSomeModal.$once('v-modal::init-show', () => {
      emitted = true;
    });
    vm.$modal.show(vmSomeModal.$props.id);
    expect(emitted).toBeTruthy();
    expect(vmSomeModal.visible).toBeTruthy();
  });
  it('should receive v-modal::init-hide', () => {
    let emitted = false;
    const vmSomeModal = getModal();
    vmSomeModal.$once('v-modal::init-hide', () => {
      emitted = true;
    });
    vm.$modal.hide(vmSomeModal.$props.id);
    expect(emitted).toBeTruthy();
    expect(vmSomeModal.visible).toBeFalsy();
  });
  it('should pass $attrs to slot', () => {
    const [modal] = vm.$modal.ctx.modalContainer.$children;
    const [modalSlot] = modal.$slots.default;
    expect(modalSlot.componentInstance.$attrs).toHaveProperty('test', 'true');
    expect(modalSlot.componentInstance.$attrs).toHaveProperty('foo', 'bar');
    expect(modalSlot.componentInstance.$el.textContent).toBe('some modal bar');
  });
});
