import Vue from 'vue';
import { Modal } from './index';

declare module 'vue/types/vue' {
  interface Vue {
    readonly $modal: Modal;
  }
}
