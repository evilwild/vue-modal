Some code is taken from [vue-js-modal]('https://github.com/euvl/vue-js-modal')

To provide $modal property on Vue with TypeScript, need to be create \*.d.ts file in your project with **next content**:

```
import Vue from 'vue';
import { Modal } from '@richdpls/vue-modal';

declare module 'vue/types/vue' {
  interface Vue {
    $modal: Modal;
  }
}

```
