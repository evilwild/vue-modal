import _Vue from 'vue';
import ModalContainer from './components/ModalContainer.vue';
import { Context, Spawn, Kill, KillAll, Show, Hide, ComponentProps } from '../types/index';

export const spawnModalContainer = (ctx: Context): void => {
  const container = document.body.appendChild(document.createElement('div'));

  const vm = new _Vue({
    name: 'ModalsContainer',
    render: (h) => h(ModalContainer),
  }).$mount(container);

  ctx.modalContainer = vm.$children[0];
};

export default (Vue: typeof _Vue, options = {}) => {
  const ctx: Context = {
    modalContainer: (null as unknown) as ModalContainer,
  };

  const spawn: Spawn = (component: _Vue, componentProps: ComponentProps = {}) => {
    const payload = {
      component,
      componentProps,
    };
    ctx.modalContainer.$emit('v-modal::spawn', payload);
  };

  const kill: Kill = (modalId: string) => {
    const payload = {
      id: modalId,
    };
    ctx.modalContainer.$emit('v-modal::kill', payload);
  };

  const killAll: KillAll = () => {
    ctx.modalContainer.$emit('v-modal::kill-all');
  };

  const findModal = (modalId: string) => {
    return ctx.modalContainer.$children.findIndex((x) => {
      return x.$props.id === modalId;
    });
  };

  const show: Show = (modalId: string) => {
    const index = findModal(modalId);
    if (index !== -1) {
      ctx.modalContainer.$children[index].$emit('v-modal::init-show');
      return;
    }
    //    console.warn(
    //      '[v-modal::init-show] No spawned modal with given modalId are found. Given value: ' + modalId
    //    );
  };

  const hide: Hide = (modalId: string) => {
    const index = findModal(modalId);
    if (index !== -1) {
      ctx.modalContainer.$children[index].$emit('v-modal::init-hide');
      return;
    }
    //    console.warn(
    //      '[v-modal::init-hide] No spawned modal with given modalId are found. Given value: ' + modalId
    //    );
  };

  return {
    spawn,
    kill,
    killAll,
    show,
    hide,
    ctx,
  };
};
