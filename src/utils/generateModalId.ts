type Closure = () => string;

const generator = (id = 1): Closure => () => 'modal-' + id++;

export default generator();
