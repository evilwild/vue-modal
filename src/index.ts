import _Vue from 'vue';
import Plugin, { spawnModalContainer } from './plugin';

export default {
  install(Vue: typeof _Vue, options = {}): void {
    if (Vue.prototype.$modal) return;

    const plugin = Plugin(Vue, options);

    Object.defineProperty(Vue.prototype, '$modal', {
      get() {
        if (!plugin.ctx.modalContainer) {
          spawnModalContainer(plugin.ctx);
        }

        return plugin;
      },
    });
  },
};
